#!/bin/bash

# Usage: sh samtools_index.sh <format> <input>
# Date: 12/11/19 - Wrapper for the Galaxy project (Master BBS) by Tristan Berlin and Chloé Cerutti
# Script version: 0.1.0

# Variables:
# $1 = input
# $2 = format

# Command line:
samtools index -$2 $1

if [[ $2 == "b" ]]
then
	cat $1".bai"
fi

if [[ $2 == "c" ]]
then
	cat $1".csi"
fi