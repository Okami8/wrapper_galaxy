# Galaxy wrapper for samtools index

This wrapper is made by Berlin Tristan and Cerutti Chloé in 2020.
This is the version 0.1.0.
This wrapper is part of the command line samtools suite.

## History

### Version changes :
* v.0.1.0  : initial public release 

## Installation

### Step 1:
To install the wrapper, you need to put the following files presented below 
under the Galaxy tools folder, that is to say in the ``tools/Samtools_index`` folder:

* ``samtools_index.xml`` : the Galaxy tool definition (xml format)
* ``samtools_index.sh`` : the wrapper tool (written in bash)
* ``README.md`` : this file (markdown format)

### Step 2:
In order to use this tool on Galaxy, you need to modify the following file:

* ``galaxy/config/tool_conf.xml`` 

Indeed, this file lays out the user interface for the tool and also allows 
to link the tool to Galaxy. 
You can add these following lines in the file: 

<section name="Samtools_index" id="111119">
    <tool file="Samtools_index/samtools_index.xml" />
</section>

## Description:
Samtools index (v.1.9) allows to index a coordinate-sorted BAM file for fast random access. 
This index is needed when region arguments are used to limit samtools view and 
similar commands to particular regions of interest.

## Usage: 

### Command line:
```bash
samtools index [-bc] <in.bam> 
```

### Options:
We chose to implement these two following options:

* *-b*: generate BAI-format index for BAM files 
* *-c*: generate CSI-format index for BAM files

### Test command lines:

* Test files source: http://genoweb.toulouse.inra.fr/~sigenae/Galaxy_Formation/test-data/

```bash
samtools index -b eXpress_hits.bam
samtools index -c eXpress_hits.bam
```

