#!/usr/bin/perl -w

# perl /home/pclf/galaxy/tools/Samtools/bam_sort_2.pl /home/pclf/galaxy/database/files/000/dataset_175.dat bam yes T ATG /home/pclf/galaxy/database/files/000/dataset_236.dat

# Usage: perl samtools_sort.pl <input> <output_format> <sort_name> <tag> <tag_name> <output>
# Date: 23/12/19 - Wrapper for the Galaxy project (Master BBS) by Tristan Berlin and Chloé Cerutti
# Script version: 0.2.0

#Imports:
use strict;
use File::Basename;
use warnings;
use Config::IniFiles;

#Variables:
my $input = $ARGV[0];
my $output_format = $ARGV[1];
my $sort_name = $ARGV[2];
my $tag = $ARGV[3];
my $tag_name = $ARGV[4];
my $output = $ARGV[5];
my $cmd = '';

#Command lines:
if($sort_name eq "yes" && $tag eq "T"){
    $cmd = "(samtools sort -O $output_format -n -t $tag_name $input) > output.file";
    #Information for biologist:
    print STDOUT "Samtools sort : \n\n $cmd \n\n ";
    system $cmd;
}
elsif($sort_name eq "yes" && $tag eq "F"){ 
    $cmd = "(samtools sort -O $output_format -n $input) > output.file"; 
    #Information for biologist:
    print STDOUT "Samtools sort : \n\n $cmd \n\n ";
    system $cmd;
}
elsif($sort_name eq "no" && $tag eq "T"){
    $cmd = "(samtools sort -O $output_format -t $tag_name $input) > output.file";
    #Information for biologist:
    print STDOUT "Samtools sort : \n\n $cmd \n\n ";
    system $cmd;
}
elsif($sort_name eq "no" && $tag eq "F"){
    $cmd = "(samtools sort -O $output_format $input) > output.file";
    #Information for biologist:
    print STDOUT "Samtools sort : \n\n $cmd \n\n ";
    system $cmd;
}

#Get output file for Galaxy:
if(! -e "output.file"){print STDERR "Pas de fichier produit \n";}
else {
    `cp -a output.file $output`;
}