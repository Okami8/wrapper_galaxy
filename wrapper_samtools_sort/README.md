# Galaxy wrapper for samtools sort

This wrapper is made by Berlin Tristan and Cerutti Chloe in 2020.
This is the version 0.1.0.
This wrapper is part of the command line samtools suite.

## History

### Script version changes :
* v.0.1.0 : initial public release (bash)
* v.0.2.0 : translation to perl

### xml version changes :
* v.0.1.0  : initial public release 

## Installation

### Step 1:
To install the wrapper, you need to put the following files presented below 
under the Galaxy tools folder, that is to say in the ``tools/Samtools_sort`` folder:

* ``samtools_sort.xml`` : the Galaxy tool definition (xml format)
* ``samtools_sort.sh`` : the wrapper tool (written in bash)
* ``samtools_sort.pl`` : the wrapper tool (written in perl)
* ``README.md`` : this file (markdown format)

### Step 2:
In order to use this tool on Galaxy, you need to modify the following file:

* ``galaxy/config/tool_conf.xml`` 

Indeed, this file lays out the user interface for the tool and also allows 
to link the tool to Galaxy. 
You can add these following lines in the file: 

<section name="Samtools_sort" id="111119">
    <tool file="Samtools_sort/samtools_sort.xml"/>
</section>

## Description:
Samtools sort (v.1.9) allows to sort alignments by leftmost coordinates, or 
by read name when -n is used. The sorted output is written to standard output 
by default, or to the specified file (out.bam) when -o is used. 

## Usage: 

### Command line:

```bash
samtools sort [-O format] [-n] [-t tag] [in.sam|in.bam]
```

### Options:

We chose to implement these two following options:

* *-n*: sort by read name (the QNAME field) rather than by chromosomal coordinates
* *-t TAG*: sort first by the value in the alignment tag TAG, then by position or 
            name (if also using -n)
* *-O FORMAT*: write the final output as sam or bam : 
      --output-fmt FORMAT[,OPT[=VAL]]... 
               Specify output format (SAM, BAM)
      --output-fmt-option OPT[=VAL]
               Specify a single output file format option in the form
               of OPTION or OPTION=VALUE
      --reference FILE
               Reference sequence FASTA FILE [null]

### Test command lines:

* Test files source: http://genoweb.toulouse.inra.fr/~sigenae/Galaxy_Formation/test-data/

```bash
samtools sort -O bam -n -t ATG eXpress_hits.bam

samtools sort -O bam -n eXpress_hits.bam

samtools sort -O bam eXpress_hits.bam -t ATG eXpress_hits.bam

samtools sort -O bam eXpress_hits.bam 
```