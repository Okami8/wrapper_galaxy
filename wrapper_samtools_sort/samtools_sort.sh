#!/bin/bash

# Usage: sh samtools_sort.sh $input $output_format $sort_name $tag_cond.tag $tag_cond.tag_name > $output
# Date: 21/11/19 - Wrapper for the Galaxy project (Master BBS) by Tristan Berlin and Chloé Cerutti
# Script version: 0.1.0

# Variables:
# $1 = input
# $2 = format output file name 
# $3 = validation sort_name
# $4 = tag_cond.tag (T/F), sort by length
# $5 = tag_cond.tag_name (tag used to sort)

# Command lines:
if [ $3 = "yes" ] && [ $4 = "T" ]
then
    samtools sort -O $2 -n -t $5 $1 
fi

if [ $3 = "yes" ] && [ $4 = "F" ]
then 
    samtools sort -O $2 -n $1 
fi

if [ $3 = "no" ] && [ $4 = "T" ]
then 
    samtools sort -O $2 -t $5 $1
fi

if [ $3 = "no" ] && [ $4 = "F" ]
then 
    samtools sort -O $2 $1
fi